# graphql-learning
A demo on how to use graphql using spring boot graphql lib, the application can run using java 16+, but it indeed built on top of `JDK 17
`


## Getting started
To start the application you need to start first docker using `docker-compose up`

Once DB is up you can start the application using `./mvnw spring-boot:run` or using the IDE itself

Postgresql Database connection string `jdbc:postgresql://localhost:5432/sales-db` username/password `db_user/123qwe`

The application will automatically populate the DB for you using `flyway` migration tool.

## Testing Examples
```
curl --location 'localhost:8080/graphql-learning' \
--header 'Content-Type: application/json' \
--data-raw '{"query":"query {customerByEmail(email: \"tkimps@sciencedirect.com\") {   firstName   lastName   orders {   orderId   orderLines {   quantity   products {   price   productId   name   }   }   }  }  }","variables":{}}'
```

Query body formatted
```
query {
    customerByEmail(email: "tkimps@sciencedirect.com") {
        firstName
        lastName
        orders {
            orderId
            orderLines {
                quantity
                products {
                    price
                    productId
                    name
                }
            }
        }
    }
}
```