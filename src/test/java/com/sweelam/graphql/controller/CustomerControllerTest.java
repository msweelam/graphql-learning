package com.sweelam.graphql.controller;

import com.sweelam.graphql.entity.CustomerEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.graphql.test.tester.GraphQlTester;
import org.springframework.test.context.ActiveProfiles;


@SpringBootTest
@AutoConfigureGraphQlTester
@ActiveProfiles("test")
class CustomerControllerTest {
    @Autowired
    GraphQlTester graphQlTester;


    @Test
    void testCustomerShouldReturnAllCustomers() {
        String document = """
                query {
                  customers {
                        id
                        firstName
                        lastName
                        email
                	}
                }
                """;

        graphQlTester.document(document)
                .execute()
                .path("customers")
                .entityList(CustomerEntity.class)
                .hasSizeGreaterThan(0);
    }


    @Test
    void testCustomerByEmailShoudReturnOnlyOneCustomer() {
        String document = """
                query customerByEmail($email: String) {
                  customerByEmail(email: $email) {
                        id
                        firstName
                        lastName
                        email
                	}
                }
                """;


        graphQlTester.document(document)
                .variable("email", "md@gmail.com")
                .execute()
                .path("customerByEmail")
                .entity(CustomerEntity.class)
                .satisfies(response -> {
                    Assertions.assertEquals("Mohamed", response.getFirstName());
                    Assertions.assertEquals("Sweelam", response.getLastName());
                });
    }
}