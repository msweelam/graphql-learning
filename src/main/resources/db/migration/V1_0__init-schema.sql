-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE SALESPEOPLE_seq;

CREATE TABLE IF NOT EXISTS SALESPEOPLE (
    SALESPERSON_ID BIGINT PRIMARY KEY DEFAULT NEXTVAL ('SALESPEOPLE_seq'),
    FIRST_NAME VARCHAR(64),
    LAST_NAME VARCHAR(64),
    EMAIL VARCHAR(128) UNIQUE,
    PHONE VARCHAR(32),
    ADDRESS VARCHAR(256),
    CITY VARCHAR(64),
    STATE CHAR(2),
    ZIPCODE VARCHAR(12)
    );

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS PRODUCTS (
    PRODUCT_ID VARCHAR(32) PRIMARY KEY,
    NAME VARCHAR(128),
    SIZE INT,
    VARIETY VARCHAR(32),
    PRICE NUMERIC(4,2),
    STATUS VARCHAR(16)
    );

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE CUSTOMERS_seq;

CREATE TABLE IF NOT EXISTS CUSTOMERS (
    CUSTOMER_ID BIGINT PRIMARY KEY DEFAULT NEXTVAL ('CUSTOMERS_seq'),
    FIRST_NAME VARCHAR(64),
    LAST_NAME VARCHAR(64),
    EMAIL VARCHAR(128) UNIQUE,
    PHONE VARCHAR(32),
    ADDRESS VARCHAR(256),
    CITY VARCHAR(64),
    STATE CHAR(2),
    ZIPCODE VARCHAR(12)
    );

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS ORDERS (
    ORDER_ID VARCHAR(16) PRIMARY KEY,
    CUSTOMER_ID BIGINT,
    SALESPERSON_ID BIGINT
    );

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE ORDER_LINES_seq;

CREATE TABLE ORDER_LINES(
                            ORDER_LINE_ID BIGINT PRIMARY KEY DEFAULT NEXTVAL ('ORDER_LINES_seq'),
                            ORDER_ID VARCHAR(16),
                            PRODUCT_ID VARCHAR(32),
                            QUANTITY INT
);

ALTER TABLE ORDERS ADD FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS(CUSTOMER_ID);
ALTER TABLE ORDERS ADD FOREIGN KEY (SALESPERSON_ID) REFERENCES SALESPEOPLE(SALESPERSON_ID);
ALTER TABLE ORDER_LINES ADD FOREIGN KEY (ORDER_ID) REFERENCES ORDERS(ORDER_ID);
ALTER TABLE ORDER_LINES ADD FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCTS(PRODUCT_ID);
