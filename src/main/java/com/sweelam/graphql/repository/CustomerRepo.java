package com.sweelam.graphql.repository;

import com.sweelam.graphql.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepo extends JpaRepository<CustomerEntity, Long> {
    Optional<CustomerEntity> findCustomerEntitiesByEmail(String email);
}
