package com.sweelam.graphql.entity;

import jakarta.persistence.*;

import java.util.Set;

@Entity
public class Orders {
    @Id
    @Column(name = "order_id")
    private String orderId;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    private Set<OrderLines> orderLines;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public Set<OrderLines> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(Set<OrderLines> orderLines) {
        this.orderLines = orderLines;
    }
}
