package com.sweelam.graphql.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "sales_people")
public class SalesPeople {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "salesPeopleSeqGen")
    @SequenceGenerator(name = "salesPeopleSeqGen", sequenceName = "SALESPEOPLE_seq", allocationSize = 1)
    @Column(name = "SALESPERSON_ID")
    private long id;

    private String firstName;

    private String lastName;

    private String email;

    private String phone;

    private String address;

    private String city;

    private String zipcode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}