package com.sweelam.graphql.service;

import com.sweelam.graphql.entity.CustomerEntity;
import com.sweelam.graphql.repository.CustomerRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private final CustomerRepo customerRepo;

    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public CustomerEntity getCustomerById(final long id) {
        return customerRepo.findById(id).orElse(null);
    }

    public CustomerEntity getCustomerByEmail(final String email) {
        return customerRepo.findCustomerEntitiesByEmail(email).orElse(null);
    }

    public List<CustomerEntity> getAllCustomers() {
        return customerRepo.findAll();
    }

    public CustomerEntity addNewCustomer(final String firstName, final String lastName, final String email) {
        var customerEntity = new CustomerEntity();
        customerEntity.setFirstName(firstName);
        customerEntity.setLastName(lastName);
        customerEntity.setEmail(email);
        return customerRepo.save(customerEntity);
    }
}
