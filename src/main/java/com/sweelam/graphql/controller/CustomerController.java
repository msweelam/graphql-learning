package com.sweelam.graphql.controller;

import com.sweelam.graphql.entity.CustomerEntity;
import com.sweelam.graphql.service.CustomerService;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @QueryMapping
    public Iterable<CustomerEntity> customers() {
        return customerService.getAllCustomers();
    }


    @QueryMapping(value = "customerById")
    public CustomerEntity getCustomerById(@Argument long id) {
        return customerService.getCustomerById(id);
    }

    @QueryMapping(value = "customerByEmail")
    public CustomerEntity getCustomerByEmail(@Argument String email) {
        return customerService.getCustomerByEmail(email);
    }

    @MutationMapping
    public CustomerEntity addCustomer(@Argument String firstName, @Argument String lastName, @Argument String email) {
        return customerService.addNewCustomer(firstName, lastName, email);
    }
}
